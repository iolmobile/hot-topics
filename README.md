
## Configuration for IOLmobile Hot Topics 

Edit the `hotTopics.yaml` file in the root of this repo.

This file will be automatically monitored by a microservice to generate the content for IOLmobile.

For example:

```yaml
label: Hot Topics

feeds:

- label: Eskom
  feed: http://www.iol.co.za/cmlink/1.1861995
  desktopLink: http://www.iol.co.za/news/special-features/eskom

- label: Operational Fiela
  feed: http://www.iol.co.za/cmlink/1.1861999
  desktopLink: http://www.iol.co.za/news/special-features/operation-fiela

- label: Xenophobic attacks 
  feed: http://www.iol.co.za/cmlink/1.1862005
  desktopLink: http://www.iol.co.za/news/special-features/xenophobic-attacks
``` 



